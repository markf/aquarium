#!/bin/bash
#
# build_database.sh - create empty temperature database schema for to log temperature in.
#
# Mark Fuller 8/7/2018
# based on work by Tom Holderness 22/01/2012

sqlite3 aquarium-logs.db 'CREATE TABLE temperature_records(unix_time BIGINT PRIMARY KEY, Tank REAL, Flow REAL, Return REAL);'
sqlite3 aquarium-logs.db 'CREATE TABLE chemistry_records(unix_time BIGINT PRIMARY KEY, pH DECIMAL(2,1), Ammonia DECIMAL(3,2), Nitrite DECIMAL(3,2), Nitrate DECIMAL(3,2), Phosphate DECIMAL(4,2), GH SMALLINT, KH SMALLINT, Copper DECIMAL(3,2));'
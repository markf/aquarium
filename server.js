// server.js - NodeJS server for the Aquarium project.

/*
Parses data from DS18B20 temperature sensor and serves as a JSON object.
Uses node-static module to serve a plot of current temperature (uses highcharts).

Refactored and updated by Mark Fuller 8/7/2018 mark.nurse@gmail.com
Based on code written by Tom Holderness 03/01/2013
Ref: www.cl.cam.ac.uk/freshers/raspberrypi/tutorials/temperature/
*/

// Load node modules
var W1Temp = require('w1temp');
var sys = require('sys');
var http = require('http');
var sqlite3 = require('sqlite3');

// Use node-static module to serve chart for client-side dynamic graph
var nodestatic = require('node-static');

// Setup static server for current directory
var staticServer = new nodestatic.Server(".");

// Setup database connection for logging
var db = new sqlite3.Database('./aquarium-logs.db');

// Setup Global Variables
var tankTemp;
var flowTemp;
var returnTemp;

// Write a temperature record in JSON format to database table.
function insertTemp(data){
   // data is a javascript object   
   var statement = db.prepare("INSERT INTO temperature_records VALUES (?, ?, ?, ?)");
   // Insert values into prepared statement
   statement.run(data.temperature_record[0].unix_time, data.temperature_record[0].Tank, data.temperature_record[0].Flow, data.temperature_record[0].Return);
   // Execute the statement
   statement.finalize();
}

// Read current temperatures from sensors
function readTemp(callback){
   // Read Tank Temperature
   W1Temp.getSensor('28-0000054e8fec').then(function (tankSensor) {
       tankTemp = tankSensor.getTemperature();
   });

   // Read Flow Temperature
   W1Temp.getSensor('28-000005890ebc').then(function (flowSensor) {
       flowTemp = flowSensor.getTemperature();
   });
   // Read Return Temperature
   W1Temp.getSensor('28-000005922f02').then(function (returnSensor) {
       returnTemp = returnSensor.getTemperature();
   });
   // Add date/time to temperatures
   var data = {
       temperature_record: [{
           unix_time: Date.now(),
           Tank: tankTemp,
           Flow: flowTemp,
           Return: returnTemp
       }]
   };
   // Execute call back with data
   callback(data);
}

// Create a wrapper function which we'll use specifically for logging
function logTemp(interval) {
  // Call the readTemp function with the insertTemp function as output to get initial reading
  readTemp(insertTemp);
  // Set the repeat interval (milliseconds). Third argument is passed as callback function to first (i.e. readTemp(insertTemp)).
  setInterval(readTemp, interval, insertTemp);
}

// Get temperature records from database
function selectTemp(num_records, start_date, callback) {
   // - Num records is an SQL filter from latest record back through time series,
   // - start_date is the first date in the time-series required,
   // - callback is the output function
   var current_temp = db.all("SELECT * FROM (SELECT * FROM temperature_records WHERE unix_time > (strftime('%s',?)*1000) ORDER BY unix_time DESC LIMIT ?) ORDER BY unix_time;", start_date, num_records,
   function (err, rows) {
       if (err) {
           response.writeHead(500, {"Content-type": "text/html"});
           response.end(err + "\n");
           console.log('Error querying database. ' + err);
           return;
       }
       data = {temperature_record: [rows]};
       callback(data);
   });
}

// Setup node http server
var server = http.createServer(
    // Our main server function
    function (request, response) {
        // Grab the URL requested by the client and parse any query options
        var url = require('url').parse(request.url, true);
        var pathfile = url.pathname;
        var query = url.query;
        // Test to see if it's a database query
        if (pathfile === '/temperature_query.json') {
            // Test to see if number of observations was specified as url query
            if (query.num_obs) {
                var num_obs = parseInt(query.num_obs);
            }
            else {
                // If not specified default to 20. Note use -1 in query string to get all.
                var num_obs = -1;
            }
            if (query.start_date) {
                var start_date = query.start_date;
            }
            else {
                var start_date = '1970-01-01T00:00';
            }
            // Send a message to console log
            console.log('Database query request from ' + request.connection.remoteAddress + ' for ' + num_obs + ' records from ' + start_date + '.');
            // call selectTemp function to get data from database
            selectTemp(num_obs, start_date, function (data) {
                response.writeHead(200, {"Content-type": "application/json"});
                response.end(JSON.stringify(data), "ascii");
            });
        }
        // Test to see if it's a request for current temperature
        if (pathfile === '/temperature_now.json') {
            readTemp(function (data) {
                response.writeHead(200, {"Content-type": "application/json"});
                response.end(JSON.stringify(data), "ascii");
            });
            // Handler for favicon.ico requests
            if (pathfile === '/favicon.ico') {
                response.writeHead(200, {'Content-Type': 'image/x-icon'});
                response.end();
            }
            else {
                // Print requested file to terminal
                console.log('Request from ' + request.connection.remoteAddress + ' for: ' + pathfile);
                // Serve file using node-static
                staticServer.serve(request, response, function (err, result) {
                    if (err) {
                        // Log the error
                        sys.error("Error serving " + request.url + " - " + err.message);
                        // Respond to the client
                        response.writeHead(err.status, err.headers);
                        response.end('Error 404 - file not found');
                    }
                });
            }
        }
    }
);
// Start temperature logging (every 5 min).

var msecs = (60 * 5) * 1000; // log interval duration in milliseconds
var secs = msecs / 1000;
logTemp(msecs);
// Send a message to console
console.log('Server is logging to database at ' + secs + 's intervals');
// Enable server
server.listen(8080);
// Log message
console.log('Server running at http://192.168.43.102:8080');